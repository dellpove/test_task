package ru.test.repo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.test.config.DBConfig;
import ru.test.config.ApplicationConfig;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

public class JdbcBaseRepo {

    private ApplicationConfig applicationConfig = new ApplicationConfig();
    private static final Logger logger = LoggerFactory.getLogger(JdbcBaseRepo.class);
    private DBConfig dbConfig = new DBConfig(applicationConfig.getDataDir() + "/config.properties");
    public JdbcBaseRepo() throws IOException { }

    protected Connection createConnection(){

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbConfig.getUrl());
            logger.debug("Connection created");
            return connection;
        } catch (Exception e) {
            logger.error("Connection error :", e);
        }
        return connection;
    }

    protected void closeConnection(Connection connection) {
        try
        {
            if(connection != null) {
                connection.close();
                logger.debug("Database connection is closed.");
            }
        }
        catch (SQLException e) {
            logger.error("Closing database connection error: ", e);
        }
    }

    private void deleteAll() throws SQLException {
        Connection connection = null;
        try {
            logger.debug("Deleting all records from test table - Begin");
            connection = createConnection();
            Statement st = connection.createStatement();
            st.execute("DELETE FROM test");
            logger.debug("Deleting all records from test table - Done!");
        } catch (Exception e) {
            logger.error("Error: ",e);
        } finally {
            closeConnection(connection);
        }
    }

    public void addBatch(int N) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            deleteAll();
            connection = createConnection();
            connection.setAutoCommit(false);
            logger.debug("Fields batch inserting - Begin");
            preparedStatement = connection.prepareStatement("INSERT INTO " +
                    "test(field) VALUES (?)");
            int index = 0;
            for (int i = 1; i < N+1; i++)
            {
                preparedStatement.setInt(1, i);
                preparedStatement.addBatch();
                if (++index % applicationConfig.getBATCH_SIZE() == 0) {
                    preparedStatement.executeBatch();
                    logger.debug("Fields batch insert #" + index);
                }
            }
            preparedStatement.executeBatch();
            connection.commit();
            logger.debug("Fields batch insert #" + index);
        }
        catch (SQLException e) {
            logger.error("Error: ",e);
            connection.rollback();
        }finally {
            closeConnection(connection);
        }
    }

    public ArrayList<Integer> readField(){
        Connection connection = null;
        Date date = new Date();
        ArrayList<Integer> elements = new ArrayList<Integer>();
        try{
            connection = createConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT FIELD FROM TEST");
            ResultSet resultSet = preparedStatement.executeQuery();
            logger.debug("Record start time: " + date.getHours()+ ":" + date.getMinutes()+ ":" + date.getSeconds() + " ");
            while (resultSet.next())
                elements.add(resultSet.getInt("field"));
            logger.debug("Record end time: " + date.getHours()+ ":" + date.getMinutes()+ ":" + date.getSeconds()+ " ");
        }catch (Exception e){
            logger.error("Error :", e);
        }finally {
            closeConnection(connection);
        }
        return elements;
    }

}
