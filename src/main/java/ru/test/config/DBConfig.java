package ru.test.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DBConfig {
    private String url;
    private String user;
    private String password;

    private Logger logger = LoggerFactory.getLogger(DBConfig.class);

    public DBConfig() throws IOException { loadProps(new File("config.properties")); }
    public DBConfig(File file) throws IOException { loadProps(file); }
    public DBConfig(String fileName) throws IOException { loadProps(new File(fileName)); }

    protected void loadProps(File file) throws IOException {
        try
        {
            Properties props = new Properties();
            props.load(new FileInputStream(file));
            url = props.getProperty("DATABASE_URL");
            user = props.getProperty("DATABASE_USER");
            password = props.getProperty("DATABASE_PASSWORD");
        }
        catch (IOException e){
            logger.error("Database config reading error: ", e);
            throw e;
        }
    }

    //----------------------------------------------
    public String getUrl() {
        return url;
    }
    public String getUser() {
        return user;
    }
    public String getPassword() {
        return password;
    }
}
