package ru.test.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationConfig {
    private String dataDir;
    private String firstXMLName;
    private String secondXMLName;
    private int BATCH_SIZE;
    private int NUMBER_N;

    private Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);

    public ApplicationConfig() throws IOException {
        loadProps(ApplicationConfig.class.getClassLoader().getResourceAsStream("config.properties"));
    }
    public ApplicationConfig(File file) throws IOException { loadProps(file); }
    public ApplicationConfig(String fileName) throws IOException { loadProps(new File(fileName)); }

    protected void loadProps(InputStream inputStream) throws IOException {
        try
        {
            Properties props = new Properties();
            props.load(inputStream);
            dataDir = props.getProperty("DATA_DIRECTORY");
            firstXMLName = props.getProperty("FIRST_XML_NAME");
            secondXMLName = props.getProperty("SECOND_XML_NAME");
            BATCH_SIZE = Integer.parseInt(props.getProperty("BATCH_SIZE"));
            NUMBER_N = Integer.parseInt(props.getProperty("NUMBER_N"));
        }
        catch (IOException e){
            logger.error("Data config reading error: ", e);
            throw e;
        }
    }

    protected void loadProps(File file) throws IOException {
        try
        {
            loadProps(new FileInputStream(file));
        }
        catch (IOException e){
            logger.error("Data config reading error: ", e);
            throw e;
        }
    }

    public String getDataDir() {
        return dataDir;
    }
    public String getFirstXMLName() {
        return firstXMLName;
    }
    public String getSecondXMLName() {
        return secondXMLName;
    }
    public int getBATCH_SIZE() {
        return BATCH_SIZE;
    }
    public int getNUMBER_N() {
        return NUMBER_N;
    }

}
