package ru.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.test.config.ApplicationConfig;
import ru.test.parsers.SumFromXMLFileCalculator;
import ru.test.parsers.XMLFileWriters;
import ru.test.repo.JdbcBaseRepo;

public class Main {

    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(Main.class);
        try {
            ApplicationConfig applicationConfig = new ApplicationConfig();
            JdbcBaseRepo jdbcBaseRepo = new JdbcBaseRepo();
            XMLFileWriters XMLFileWriters = new XMLFileWriters();
            SumFromXMLFileCalculator sumFromXMLFileCalculator = new SumFromXMLFileCalculator();
            jdbcBaseRepo.addBatch(applicationConfig.getNUMBER_N());
            XMLFileWriters.createFirstXML(jdbcBaseRepo.readField(), applicationConfig.getFirstXMLName());
            XMLFileWriters.createSecondXML();
            sumFromXMLFileCalculator.calculateArithmeticSum(applicationConfig.getSecondXMLName());
        }
        catch (Exception e) {
            logger.error("Error : ",e);
        }
    }
}
