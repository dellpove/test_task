package ru.test.parsers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.test.config.ApplicationConfig;

public class XMLFileWriters {

    private ApplicationConfig applicationConfig = new ApplicationConfig();
    Logger logger = LoggerFactory.getLogger(XMLFileWriters.class);
    public XMLFileWriters() throws IOException { }

    public void createFirstXML(ArrayList<Integer> fields , String firstXMLName) {
        try {
            ArrayList<Integer> temp;
            temp = fields;
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder().newDocument();

            Element entries = document.createElement("entries");
            document.appendChild(entries);

            for(int i = 0; i < temp.size();i++) {
                Element entry = document.createElement("entry");
                entries.appendChild(entry);

                Element field = document.createElement("field");
                String tempStr = "" + temp.get(i);
                field.setTextContent(tempStr);
                entry.appendChild(field);
            }
            Transformer transformer = TransformerFactory.newInstance()
                    .newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(
                    new File(System.getProperty("user.dir")
                            + File.separator + firstXMLName));

            transformer.transform(source, result);
            logger.debug("1.xml was saved!");
        } catch (Exception e) {
            logger.error("Error :",e);
        }
    }

    public void createSecondXML(){
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer(new StreamSource(applicationConfig.getDataDir() + "/1.xsl"));
            transformer.transform(
                    new StreamSource("1.xml"),
                    new StreamResult(new File(System.getProperty("user.dir")
                            + File.separator + applicationConfig.getSecondXMLName())));
            logger.debug("2.xml сохранен!");
        } catch(Exception e) {
            logger.debug("Error: ", e);
        }
    }
}


