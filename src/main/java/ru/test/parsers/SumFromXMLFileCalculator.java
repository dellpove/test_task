package ru.test.parsers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;

public class SumFromXMLFileCalculator {
    Logger logger = LoggerFactory.getLogger(SumFromXMLFileCalculator.class);

    public SumFromXMLFileCalculator() throws IOException { }

    public void calculateArithmeticSum(String secondXMLFile) {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(System.getProperty("user.dir")
                    + File.separator + secondXMLFile);
            Node root = document.getDocumentElement();
            if (root.hasChildNodes()) {
                NodeList entries = root.getChildNodes();
                int sum = 0;
                for (int i =0; i<entries.getLength();i++){
                    if(entries.item(i).hasAttributes()){
                        sum+=Integer.parseInt(entries.item(i).getAttributes().item(0).getNodeValue());
                    }
                }
                logger.debug("Arithmetic Sum: " + sum);
            }
        } catch (Exception e) {
            logger.error("Error: ", e);
        }
    }
}